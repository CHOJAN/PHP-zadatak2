﻿index.php i register.php dopuniti tako da: 
1) Sadrži polje za unos datuma rođenja, za ovo polje napraviti validaciju i poruku za grešku za slučaj da nije popunjeno.
2) Na register2.php stranici dodati ispis datuma i dodati da se ispiše i dan u nedelji na engleskom jeziku.
3) Napraviti dodatnu proveru za dužinu imena i prezimena i email adrese. Minimalna dužina imena i prezimena je 2 znaka, a email adrese 6 znakova.
4) Napraviti dodatnu proveru email adrese, pri čemu je kriterijum da email adresa pripada domenu \@gmail.com. U suprotnom ispisati odgovarajuću grešku. Za potrebe provere email adrese napraviti funkciju checkEmailAdress koja kao parametar prihvata email adresu, a kao rezultat vraća vrednost logičkog tipa - true ili false. (edited)