<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PHP-Domaci2</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
  <?php
  $name = $email = $phone = $birthdate = $gender = $comment = "";
  $nameErr = $emailErr = $phoneErr = $birthdateErr = $genderErr = "";
  
  if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['email'])) { $email = $_GET['email']; }
    if (isset($_GET['phone'])) { $phone = $_GET['phone']; }
    if (isset($_GET['birthdate'])) { $birthdate = $_GET['birthdate']; }
    if (isset($_GET['gender'])) { $gender = $_GET['gender']; }
    if (isset($_GET['comment'])) { $comment = $_GET['comment']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['emailErr'])) { $emailErr = $_GET['emailErr']; }
    if (isset($_GET['phoneErr'])) { $phoneErr = $_GET['phoneErr']; }
    if (isset($_GET['birthdateErr'])) { $birthdateErr = $_GET['birthdateErr']; }
    if (isset($_GET['genderErr'])) { $genderErr = $_GET['genderErr']; }
    if (isset($_GET['commentErr'])) { $commentErr = $_GET['commentErr']; }

  ?>


<form action="register.php" method="post">

  <div>
    <label for="name">Full name <span class="required">* <?php echo $nameErr;?></span></label><br>
    <input type="text" name="name" value="<?php echo $name;?>">
  </div>

  <div>
    <label for="mail">Email <span class="required">* <?php echo $emailErr;?></span></label><br>
    <input type="email" id="mail" name="email" value="<?php echo $email;?>">
  </div>

  <div>
    <label for="phone">Phone number <span class="required">* <?php echo $phoneErr;?></span></label><br>
    <input type="text" name="phone" value="<?php echo $phone;?>">
  </div>

  <div>
    <label for="birthdate">Date of birth <span class="required">* <?php echo $birthdateErr;?></span></label><br>
    <input type="date" name="birthdate" value="<?php echo $birthdate;?>">
  </div>

  <div>
    <label for="gender">Gender <span class="required">*</span></label>
    <input class="inputgender" type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female">Female
    <input class="inputgender" type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male">Male
    <span class="required"><?php echo $genderErr;?></span>
  </div>
  <div>
    <label for="comment">Your message:</label><br>
    <textarea name="comment" rows="10" cols="45"><?php echo $comment;?></textarea>
  </div>
 
  <div>
    <button type="submit">Submit</button>
  </div>

</form>



</body>
</html>