<?php

$name = $email = $phone = $birthdate = $gender = $comment = "";
$nameErr = $emailErr = $phoneErr = $birthdateErr = $genderErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Full name is required";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
      if (strlen($name)<4) {
        $nameErr = "Minimum number of letters is 4";
      }
  }

  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
    elseif (checkEmailAddress($email)==false) {
      $emailErr = "Must be gmail address";
    }
    elseif (strlen($email)<16) {
      $emailErr = "Short Email address";
    }
  }

  if (empty($_POST["phone"])) {
    $phoneErr = "Phone number is required";
  } else {
    $phone = test_input($_POST["phone"]);
    // check if phone number is well-formed
    if (!preg_match("/^[0-9]*$/",$phone)) {
      $phoneErr = "Only numbers are allowed"; 
    }
  }
    
  if (empty($_POST["birthdate"])) {
    $birthdateErr = "Date of birth is required";
  } else {
    $birthdate = test_input($_POST["birthdate"]);
    // check if birthdate is possible
    $date=date("Y-m-d");
    if($date<$birthdate) {
    $birthdateErr = "You came from the future";
    }
  }
  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (!empty($nameErr) or !empty($emailErr) or !empty($phoneErr) or !empty($birthdateErr) or !empty($genderErr) or !empty($commentErr)) {
  $params = "name=" . urlencode($_POST["name"]);
  $params .= "&email=" . urlencode($_POST["email"]);
  $params .= "&phone=" . urlencode($_POST["phone"]);
  $params .= "&birthdate=" . urlencode($_POST["birthdate"]);
  $params .= "&gender=" . urlencode($_POST["gender"]);
  $params .= "&comment" . urlencode($_POST["comment"]);

  $params .= "&nameErr=" . urlencode($nameErr);
  $params .= "&emailErr=" . urlencode($emailErr);
  $params .= "&phoneErr=" . urlencode($phoneErr);
  $params .= "&birthdateErr=" . urlencode($birthdateErr);
  $params .= "&genderErr=" . urlencode($genderErr);
  $params .= "&commentErr" . urlencode($commentErr);

  header("Location: index.php?" . $params);
  }  
  else {

  echo "<h2>What you see is what we get:</h2>";
  echo "Name: " .$_POST['name'];
  echo "<br>";
  echo "Email: " .$_POST['email'];
  echo "<br>";
  echo "Phone: " .$_POST['phone'];
  echo "<br>";
  echo "Birthdate: " .$_POST['birthdate'];
  echo "<br>";
  echo "Gender: " .$_POST['gender'];
  echo "<br>";
  echo "Comment: " .$_POST['comment'];
  echo "<br>";
  echo "You were born on " . date("l",strtotime($birthdate));
  echo "<br><br>";

  echo "<a href=\"index.php\">Return to form</a>";
  }
}

function checkEmailAddress($email) {
  if (!strpos($email,'@gmail.com')) {
    return false; 
  } else {
    return true;
  }
  }

function test_input($data) {
$data = trim($data);
$data = stripslashes($data);
$data = htmlspecialchars($data);
return $data;
}

?>